<?php

declare(strict_types=1);

namespace Command\Render;

/**
 * Class ARenderer
 * @package Command\Render
 */
abstract class ARenderer
{
    public string $prefix;
    public string $postfix;
    public string $normal;

    public string $black;
    public string $red;
    public string $green;
    public string $yellow;
    public string $blue;
    public string $purple;
    public string $cyan;
    public string $white;

    public string $bg_black;
    public string $bg_red;
    public string $bg_green;
    public string $bg_yellow;
    public string $bg_blue;
    public string $bg_purple;
    public string $bg_cyan;
    public string $bg_white;

    public string $bold;
    public string $underline;
    public string $flash;
    public string $invert;

    /**
     * @param string $message
     * @param bool $color
     * @return string
     */
    public function getFormatted(string $message, bool $color = true) : string
    {
        if ($color === false) return strip_tags($message);
        $currentFormats = [];
        $result = "";
        $messageParts = preg_split('/(<\/?[a-z_0-9]+\/?>)/i', $message, -1, PREG_SPLIT_DELIM_CAPTURE);
        if ($messageParts === false) return $message;
        foreach ($messageParts as $part) {
            if (!preg_match('/<(\/?)([a-z_0-9]+)(\/?)>/i',$part,$matches) || !isset($this->{$matches[2]})) {
                $result .= $part;
                continue;
            }
            $format = $this->{$matches[2]};
            if ($matches[3] === '/') {
                $result .= $this->prefix.$format;
                continue;
            }
            if ($matches[1] !== '/') {
                $currentFormats[$format] = null;
            } else {
                unset($currentFormats[$format]);
                $result .= $this->prefix.$this->normal.$this->postfix;
            }
            foreach ($currentFormats as $format => $null) {
                $result .= $this->prefix.$format.$this->postfix;
            }
        }
        return $result;
    }
}