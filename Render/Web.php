<?php

declare(strict_types=1);

namespace Command\Render;

/**
 * Class Web
 * @package Command\Render
 */
class Web extends ARenderer
{
    public string $prefix = "";
    public string $postfix = "";
    public string $normal = "</span>";

    public string $black = "<span style='color: black;'>";
    public string $red = "<span style='color: red;'>";
    public string $green = "<span style='color: green;'>";
    public string $yellow = "<span style='color: yellow;'>";
    public string $blue = "<span style='color: blue;'>";
    public string $purple = "<span style='color: purple;'>";
    public string $cyan = "<span style='color: cyan;'>";
    public string $white = "<span style='color: white;'>";

    public string $bg_black = "<span style='background-color: black;'>";
    public string $bg_red = "<span style='background-color: red;'>";
    public string $bg_green = "<span style='background-color: green;'>";
    public string $bg_yellow = "<span style='background-color: yellow;'>";
    public string $bg_blue = "<span style='background-color: blue;'>";
    public string $bg_purple = "<span style='background-color: purple;'>";
    public string $bg_cyan = "<span style='background-color: cyan;'>";
    public string $bg_white = "<span style='background-color: white;'>";

    public string $bold = "<span style='font-weight: bold;'>";
    public string $underline = "<span style='text-decoration: underline;'>";
    public string $flash = "<span style='text-decoration: blink;'>";
    public string $invert = "<span style=''>";
}