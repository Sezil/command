<?php

declare(strict_types=1);

namespace Command\Render;

/**
 * Class Console
 * @package Command\Render
 */
class Console extends ARenderer
{
    public string $prefix = "\33[";
    public string $postfix = "m";
    public string $black = "30";
    public string $red = "31";
    public string $green = "32";
    public string $yellow = "33";
    public string $blue = "34";
    public string $purple = "35";
    public string $cyan = "36";
    public string $white = "37";
    public string $normal = "0";

    public string $bg_black = "40";
    public string $bg_red = "41";
    public string $bg_green = "42";
    public string $bg_yellow = "43";
    public string $bg_blue = "44";
    public string $bg_purple = "45";
    public string $bg_cyan = "46";
    public string $bg_white = "47";

    public string $bold = "1";
    public string $underline = "4";
    public string $flash = "5";
    public string $invert = "7";

    public string $delete5 = "5D";
    public string $rowback = "1A";
    public string $delete = "K";
}