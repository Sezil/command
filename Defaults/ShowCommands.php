<?php

declare(strict_types=1);

namespace Command\Defaults;

use Command\Base\ABaseCommand;

/**
 * Class ShowCommands
 * @package Command\Defaults
 */
class ShowCommands extends ABaseCommand
{
    public static function getCallAs() : string
    {
        return 'commands';
    }

    public static function getDescription() : string
    {
        return 'Show available commands';
    }

    public function getHelpUsage() : void
    {
        $this->writeLn('Usage: <green>'. self::getCallAs().'</green>');
        $this->writeLn('       <green>'. self::getCallAs().' [namespace]</green>');
        $this->writeLn('       <green>'. self::getCallAs().' [namespace].[namespace]...</green>');
    }

    public function execute() : int
    {
        $this->writeHeader();
        $this->printCommands();
        $this->writeFooter();
        return 0;
    }

    public function printCommands() : void
    {
        $namespace = '';
        if (!empty($this->_unknownParams)) {
            $namespace = implode('.',$this->_unknownParams);
            $this->writeLn('<green>Commands for namespace '.$namespace.':</green>');
            $commands = $this->_cliCommand->getMappedCommand($namespace,'commands');
            $namespace .= '.';
        } else {
            $this->writeLn('<green>Default:</green>');
            $commands = $this->_cliCommand->getCommands();
        }

        foreach ($commands as $command => $value) {
            $command = $namespace.$command;
            if (is_array($value)) {
                $this->writeLn('<green>'.$command.':</green>');
                foreach ($value as $k => $item) {
                    $this->writeLn(' - <cyan>'.$command.'.'.$k.'</cyan>');
                }
            } else {
                $this->writeLn(' - <cyan>'.$command.'</cyan>');
            }
        }
    }
}