<?php

declare(strict_types=1);

namespace Command\Defaults;

use Command\Base\ABaseCommand;
use Command\Render\Console;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class FormatExamples
 * @package Command\Defaults
 */
class FormatExamples extends ABaseCommand
{
    public static function getCallAs() : string
    {
        return 'formats';
    }

    public static function getDescription() : string
    {
        return 'Show available output formats';
    }

    public function execute() : int
    {
        $this->writeHeader();

        $r = new ReflectionClass(Console::class);
        $properties = $r->getProperties(ReflectionProperty::IS_PUBLIC);
        foreach ($properties as $property) {
            if (in_array($property->name, ['prefix', 'postfix'])) continue;
            $test = '<' . $property->name . '>Formatted output</' . $property->name . '> : ';
            echo $test;
            $this->writeLn($test);
        }

        $this->writeFooter();
        return 0;
    }
}