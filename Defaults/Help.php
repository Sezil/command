<?php

declare(strict_types=1);

namespace Command\Defaults;

use Command\Base\ABaseCommand;

/**
 * Class Help
 * @package Command\Defaults
 */
class Help extends ABaseCommand
{
    public static function getCallAs() : string
    {
        return 'help';
    }

    public static function getDescription() : string
    {
        return 'Show help';
    }

    public function execute() : int
    {
        $this->_verbose = true;
        $this->writeHeader();
        if (empty($this->_unknownParams)) {
            $this->showCommands();
        } else {
            $command = $this->_cliCommand->getMappedCommand(current($this->_unknownParams));
            if (is_string($command) && class_exists($command)) {
                /** @var ABaseCommand $command */
                $command = new $command($this->_cliCommand,$this->_params);
                $command->init(false);
                $command->writeHelp();
            } else {
                $this->showCommands();
            }
        }
        $this->writeFooter();
        return 0;
    }

    protected function showCommands() : void
    {
        $commands = new ShowCommands($this->_cliCommand,$this->_params);
        $commands->init(false);
        $this->writeBoxMessage('Available commands:');
        $commands->printCommands();
    }
}