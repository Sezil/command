<?php

declare(strict_types=1);

namespace Command\Defaults;

use Command\Base\ABaseCommand;
use RuntimeException;

/**
 * Class BashCompletion
 * @package Command\Defaults
 * TODO create for Windows https://stackoverflow.com/questions/33497205/custom-powershell-tab-completion-for-a-specific-command
 */
class BashCompletion extends ABaseCommand
{
    public static function getCallAs() : string
    {
        return 'bash-completion';
    }

    public static function getDescription() : string
    {
        return 'Generates bash completion for commands';
    }

    /**
     * @throws RuntimeException
     */
    public function execute() : int
    {
        $this->writeHeader();

        if (!$this->_cliCommand->isCli()) throw new RuntimeException('This command can be run only by root.');
        $commands = $this->_cliCommand->getCommands();
        $commandOptions = [];
        foreach ($commands as $command => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $item) {
                    $commandOptions[] = $command . '.' . $k;
                }
            } else {
                $commandOptions[] = $command;
            }
        }

        $content = '_command()
{
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts="' . implode(' ',$commandOptions) . '"
    COMPREPLY=( $(compgen -W "${opts}" -- "${cur}") )
}';
        file_put_contents('/etc/bash_completion.d/command',$content);
        $this->writeLn('<cyan>To refresh bash-completion run:</cyan><invert> . /etc/bash_completion.d/command </invert>');
        $this->writeFooter();
        return 0;
    }
}