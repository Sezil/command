<?php

declare(strict_types=1);

namespace Command\Base;

/**
 * Interface ICommand
 * @package Command\Base
 */
interface ICommand
{
    public static function getCallAs() : string;

    public static function getDescription() : string;

    public function execute() : int;
}