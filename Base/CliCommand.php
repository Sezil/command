<?php

declare(strict_types=1);

namespace Command\Base;

use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * Class CliCommand
 * @package Command
 */
class CliCommand
{
    protected string $_temp = '';
    protected string $_log = '';
    protected array $_params = [];
    protected array $_mappedCommands = [];

    public function __construct()
    {
        $this->setCommandsDirectory(__DIR__.'/../Defaults');
    }

    public function setTempDirectory(string $dir) : static
    {
        if (!is_dir($dir)) throw new RuntimeException('Path: "'.$dir.'" is not a valid directory.');
        $this->_temp = $dir;
        return $this;
    }

    public function setLogDirectory(string $dir) : static
    {
        $dir .= '/';
        if (!is_dir($dir)) throw new RuntimeException('Path: "'.$dir.'" is not a valid directory.');
        $this->_log = $dir;
        return $this;
    }

    public function setCommandsDirectory(string $dir) : static
    {
        if (!is_dir($dir)) throw new RuntimeException('Path: "'.$dir.'" is not a valid directory.');
        $this->_mappedCommands = array_merge($this->_mappedCommands, $this->getMappedCommands($dir));
        return $this;
    }

    public function getCommands() : array
    {
        return $this->_mappedCommands;
    }

    public function execute(array $params = []) : void
    {
        $command = array_shift($params);
        $commandClass = $this->getMappedCommand($command);
        $params[] = $command;
        if (is_array($commandClass)) {
            $commandClass = $this->_mappedCommands['commands'];
        } elseif (!class_exists($commandClass)) {
            $commandClass = $this->_mappedCommands['help'];
        }
        $exitCode = null;
        /** @var ABaseCommand $actualCommand */
        $actualCommand = new $commandClass($this, $params);
        try {
            $actualCommand->init();
            $exitCode = $actualCommand->execute();
        } catch (RuntimeException $e) { $actualCommand->throwError($e); }
        $this->_params = $params;
        exit($exitCode ?? 0);
    }

    public function getMappedCommand(?string $command, string $default = 'help'): mixed
    {
        $keys = explode('.', (string)$command);
        $result = $this->_mappedCommands;
        foreach ($keys as $key) {
            if (!isset($result[$key])) return $this->_mappedCommands[$default];
            $result = $result[$key];
        }
        return $result;
    }

    /**
     * TODO refactor
     */
    protected function getMappedCommands(string $dir) : array
    {
        $dir = realpath($dir);
        if ($dir === false || !is_dir($dir)) return [];
        $mapped = [];
        $scanDir = scandir($dir);
        if ($scanDir === false) return [];
        foreach (array_diff($scanDir, ['.','..']) as $fileName) {
            if (is_dir($dir . DIRECTORY_SEPARATOR . $fileName)) {
                $mapped[$fileName] = $this->getMappedCommands($dir . DIRECTORY_SEPARATOR . $fileName);
            } else {
                $command = $this->getCommandByPath($dir . DIRECTORY_SEPARATOR, $fileName);
                $mapped += $command;
            }
        }
        return $mapped;
    }

    protected function getCommandByPath(string $directory, string $filename): array
    {
        if (empty($directory)) return [];
        $className = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        if (!class_exists($className)) {
            $directories = explode(DIRECTORY_SEPARATOR, $directory);
            $filename = '\\' . ucfirst((string)array_pop($directories)) . $className;
            return $this->getCommandByPath(implode(DIRECTORY_SEPARATOR, $directories), $filename);
        }

        $reflection = new ReflectionClass($className);
        if ($reflection->isSubclassOf(ABaseCommand::class) && $reflection->implementsInterface(ICommand::class) && $reflection->isInstantiable()) {
            try {
                return [$reflection->newInstanceWithoutConstructor()::getCallAs() => $filename];
            } catch (ReflectionException) {}
        }
        return [];
    }

    public function getLogDirectory() : string
    {
        if ($this->_log === '') return $this->getTempDirectory();
        return $this->_log;
    }

    public function getTempDirectory() : string
    {
        if ($this->_temp === '') return sys_get_temp_dir();
        return $this->_temp;
    }

    public function isCli(): bool
    {
        if (defined('STDIN')) {
            return true;
        }

        if (PHP_SAPI === 'cli') {
            return true;
        }

        if (array_key_exists('SHELL', $_ENV)) {
            return true;
        }

        if (empty($_SERVER['REMOTE_ADDR']) && !isset($_SERVER['HTTP_USER_AGENT']) && count($_SERVER['argv']) > 0) {
            return true;
        }

        if (!array_key_exists('REQUEST_METHOD', $_SERVER)) {
            return true;
        }

        return false;
    }
}