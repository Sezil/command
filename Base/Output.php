<?php

declare(strict_types=1);

namespace Command\Base;

use Command\Render\ARenderer;
use DateTime;
use DateTimeZone;

/**
 * Trait Output
 * @package Command
 * @method ARenderer getRenderer()
 */
trait Output
{
    protected mixed $_start;
    protected float $_progress = 0.0;
    protected float $_step = 0.0;

    public function writeLn(string $str = "") : void
    {
        $this->write($str . PHP_EOL);
    }

    public function writeAndLogLn(string $str = "") : void
    {
        $this->writeAndLog($str . PHP_EOL);
    }

    public function writeTable(array $data, array $formatStrings = [], string $separator = '#') : void
    {
        if (!$this->_verbose) return;
        $data = array_filter($data);
        $result = '';
        foreach ($data as $row) {
            $result .= '   ' . implode($separator, $row) . PHP_EOL;
        }
        $result = (string)shell_exec('echo "' . $result . '" | column -t -s\'' . $separator . '\'');

        if ($this->_color && !empty($formatStrings)) {
            $format = array_fill_keys(['before', 'after'],null);
            foreach ($formatStrings as $formatString) {
                $format['before'] .= '<' . $formatString . '>';
                $format['after'] .= '</' . $formatString . '>';
            }
            $result = $format['before'] . $result . $format['after'];
        }
        $this->write($result);
    }

    public function write(string $str) : void
    {
        if (!$this->_verbose) return;
        echo $this->getRenderer()->getFormatted($str,$this->_color);
    }

    public function writeAndLog(string $str) : void
    {
        $this->log(strip_tags($str));
        if (!$this->_verbose) return;
        echo $this->getRenderer()->getFormatted($str,$this->_color);
    }

    public function writeHeader(bool $time = false) : void
    {
        if (!$this->_verbose) return;
        $class = static::class;
        $message = '<yellow>' . strtoupper($class::getCallAs()) . ' - ' . $class::getDescription() . '</yellow>';
        if ($time) {
            $timeParts = explode('.', $this->_start);
            $message .= PHP_EOL.'<green>BEGIN: '.date('d.m.Y - H:i:s.', (int)$timeParts[0]) . $timeParts[1] . '</green>';
        } else {
            $message .= ' <invert> BEGIN </invert>';
        }
        $this->writeBoxMessage($message);
        $this->writeLn();
    }

    public function writeFooter(bool $time = false) : void
    {
        if (!$this->_verbose) return;
        $class = static::class;
        $this->writeLn();
        $message = '<yellow>' . strtoupper($class::getCallAs()) . ' - ' . $class::getDescription() . '</yellow>';
        if ($time) {
            $runTime = explode('.', (string)microtime(true) - $this->_start);
            $runTimeDate = new DateTime('@' . $runTime[0], new DateTimeZone('UTC'));
            $currentTime = explode('.', (string)microtime(true));
            $message .= PHP_EOL . '<green>END: ' . date('d.m.Y - H:i:s.', (int)$currentTime[0]) . $currentTime[1] . ' RUNTIME: ' . $runTimeDate->format('H:i:s.') . $runTime[1] . '</green>';
        } else {
            $message .= ' <invert> END </invert>';
        }
        $this->writeBoxMessage($message);
    }

    public function writeBoxMessage(string $message, string $color = 'green', string $borderChar = '#') : void
    {
        if (!$this->_verbose) return;
        $lines = explode(PHP_EOL, $message);

        $lineLengths = array_map('strlen', $lines);
        $lineWithoutTagsLengths = array_map('strlen', explode(PHP_EOL, strip_tags($message)));

        $length = max($lineWithoutTagsLengths) + 1;
        $borderLength = $length + 3;

        $this->writeLine($borderLength, $color, $borderChar);
        foreach ($lines as $row => $line) {
            $correctedLength = $length + ($lineLengths[$row] - $lineWithoutTagsLengths[$row]);
            $this->writeLn("<$color>$borderChar</$color> " . str_pad($line, $correctedLength) . "<$color>$borderChar</$color>");
        }
        $this->writeLine($borderLength, $color, $borderChar);
    }

    public function writeLine(int $length, string $color = 'green', string $borderChar = '#') : void
    {
        $this->writeLn("<$color>" . str_repeat($borderChar,$length) . "</$color>");
    }

    /**
     * Writes progressbar v % with the text from parameter
     */
    public function progressOutputStart(int $numberOfOperations, string $message = 'Progress') : void
    {
        $this->_progress = 0;
        if ($numberOfOperations !== 0) {
            $this->_step = 1 / ($numberOfOperations / 100);
        }
        $this->write(sprintf("<yellow>%s</yellow><green>     0 %%</green>", $message));

        if ($this->_step === 0.0) {
            $this->_step = 100;
            $this->progressOutputAddStep();
        }
    }

    public function progressOutputAddStep() : bool
    {
        $this->_progress += $this->_step;
        if ($this->_progress >= 99.999999) {
            $this->writeLn("<delete5/><green>" . str_pad("100", 3, ' ', STR_PAD_LEFT) . " %</green>");
            return false;
        }
        $this->write("<delete5/><green>" . str_pad((string)ceil($this->_progress), 3, ' ', STR_PAD_LEFT) . " %</green>");
        return true;
    }

    public function logLn(string $str, bool $addTimestamp = true): self
    {
        $this->log($str . PHP_EOL, $addTimestamp);
        return $this;
    }

    public function log(string $str, bool $addTimestamp = true): self
    {
        $time = '';
        if ($addTimestamp) {
            $dateTime = new DateTime();
            $time = '[' . $dateTime->format('Y-m-d H:i:s.u') . '] : ';
        }
        file_put_contents($this->_logFile, $time . $str,FILE_APPEND);
        return $this;
    }

    public function getExecutionDuration(bool $asString = true): string
    {
        if (!$asString) return microtime(true) - $this->_start;
        $durationTime = $this->getExecutionDuration(false);
        $int = floor((float)$durationTime);
        $fraction = $durationTime - $int;
        $result = (floor($int / 86400)) . ' days ';
        $int %= 86400;
        $result .= (floor($int / 3600)) . ':';
        $int %= 3600;
        $result .= (floor($int / 60)) . ':';
        $int %= 60;
        $result .= number_format($int + $fraction, 6);
        return $result;
    }
}