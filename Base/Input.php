<?php

declare(strict_types=1);

namespace Command\Base;

use Exception;
use RuntimeException;

/**
 * upgrade: http://stackoverflow.com/questions/11327367/detect-if-a-php-script-is-being-run-interactively-or-not
 * @method void write($str)
 * @method void throwError(Exception $e)
 */
trait Input
{
    public bool $noColor = false;

    /**
     * Return input from user console
     * @param string $default Default return value if empty input
     */
    protected function getInput(string $default = '') : string
    {
        if ($this->noColor) {
            $result = trim(fgets(STDIN));
        } else {
            echo "\e[7m";
            $result = trim(fgets(STDIN));
            echo "\e[0m";
        }
        if ($result === '') return $default;
        return $result;
    }



    /**
     * Write message and return input from user console
     * @param string $question Message written to console
     * @param string $defaultAnswer Default return value if empty input
     */
    protected function askUser(string $question, string $defaultAnswer = '') : string
    {
        if (!empty($defaultAnswer)) $question .= '[Default => ' . $defaultAnswer . ']:';
        $this->write($question);
        return $this->getInput($defaultAnswer);
    }

    /**
     * Write message and return one of choices determined by input from user console
     * @param string $question Message written to console
     * @param array $choices Array of returnable choices
     * @throws RuntimeException if provided with 3 wrong inputs
     */
    protected function askUserChoice(string $question, array $choices) : string
    {
        if (empty($choices)) throw new RuntimeException('Empty choices for method askUserChoice in '. static::class);
        reset($choices);
        $returnKey = false;
        if (is_string(key($choices))) $returnKey = true;
        $encoding = 'UTF-8';
        $mb_strtolower = static function($name) use ($encoding) {
            return mb_strtolower($name,$encoding);
        };
        $choicesCI = array_map($mb_strtolower,$choices);
        for ($i = 0; $i < 3; $i++) {
            $this->write($question.' ['.implode('/',$choices).']:');
            $result = array_search(mb_strtolower($this->getInput(), $encoding), $choicesCI, true);
            if ($result !== false) {
                if ($returnKey) return $result;
                return $choices[$result];
            }
        }
        throw new RuntimeException('User failed to provide valid answer. Terminating...');
    }

    protected function askUserYesNo(string $question) : bool
    {
        return in_array($this->askUserChoice($question,['y','yes','n','no']),['y','yes']);
    }
}
