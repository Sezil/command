<?php

declare(strict_types=1);

namespace Command\Base;

use Traversable;

/**
 * Class Param
 * @package Command
 */
class Param
{
    public string $alias;
    public ?string $regexp;
    public mixed $default = null;
    public bool $required = false;
    public mixed $value = null;
    public ?string $description = null;

    public function __construct(string $alias, ?string $regexp = null, mixed $default = null, bool $required = false, ?string $description = null)
    {
        $this->alias = $alias;
        if (empty($regexp)) $regexp = '--' . $alias;
        $this->regexp = $regexp;
        $this->default = $default;
        $this->required = $required;
        $this->value = $default;
        $this->description = $description;
        if (!isset($description)) $this->description = $alias;
    }

    public function setValue(mixed $value) : bool
    {
        if (preg_match('/^('.$this->regexp.')$/i',$value,$matches)) {
            if (!is_bool($this->default)) {
                $this->value = end($matches);
                if (is_object($this->default)) {
                    $class = get_class($this->default);
                    $this->value = new $class($this->value);
                }
            } else {
                $this->value = !$this->default;
            }
            return true;
        }
        return false;
    }

    public function requireCheck() : bool
    {
        if (!$this->required) return true;
        return $this->value !== null;
    }

    public function getParamConfig(bool $writable = true, bool $overwriteWithValues = false, bool $onlyDiff = false) : array
    {
        if ($onlyDiff && $this->default === $this->value) return [];
        $default = $this->default;
        if ($overwriteWithValues) $default = $this->value;
        $required = $this->required;
        if ($writable) {
            $default = $this->getStringValue($default);
            if ($required) $required = 'REQUIRED';
        }
        return [$this->alias, $this->regexp, $default, $required, $this->description];
    }

    protected function getStringValue(mixed $value) : string
    {
        if (is_string($value)) return $value;
        if (is_bool($value)) return ($value) ? 'true' : 'false';
        if ($value === null) return 'NULL';
        if (is_object($value)) {
            if (is_callable([$value, '__toString'])) return $value->__toString();
            if (is_callable([$value, 'render'])) return $value->render();
            $result = [];
            if ($value instanceof Traversable) {
                $publicVars = $value;
            } else {
                $publicVars = get_object_vars($value);
            }
            foreach ($publicVars as $property => $item) {
                $result[] = $property . ': ' . $this->getStringValue($item);
            }
            return 'Object of class: ' . get_class($value) . ' => ' . implode('|', $result);
        }
        ob_start();
        var_dump($value);
        $result = (string)ob_get_clean();
        return preg_replace('/\s+|\s$/','',$result);
    }
}