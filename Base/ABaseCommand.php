<?php

declare(strict_types=1);

namespace Command\Base;

use Command\Render\ARenderer;
use Command\Render\Console;
use RuntimeException;

/**
 * Class ABaseCommand
 * @package Command
 *
 * @property bool $_color
 * @property bool $_verbose
 * @property string $_logFile
 * @property bool $_cron
 */
abstract class ABaseCommand implements ICommand
{
    use Output;

    protected CliCommand $_cliCommand;
    protected array $_params;
    protected ARenderer $_renderer;
    /** @var Param[]  */
    protected array $_parameters = [];
    protected array $_unknownParams = [];

    public function __construct(CliCommand $cliCommand, array $params)
    {
        $this->_start = microtime(true);
        $this->_cliCommand = $cliCommand;
        $this->_params = $params;
        $this->setRenderer(new Console());
    }

    public function setUp() : void
    {
        $this->setProperty('_color','-c',true, false, 'Enable output formatting');
        $this->setProperty('_verbose','-v',true, false, 'Enable output');
        $logFile = $this->_cliCommand->getLogDirectory() . 'command_' . static::getCallAs() . '_log';
        $this->setProperty('_logFile','-l=(.*)',$logFile, false, 'Sets the log file');
        $this->setBoolProperty('_cron', false, false, 'Sets crone mode - no verbose and color');
    }

    public function init(bool $require = true) : void
    {
        $this->setUp();
        $this->parse($require);
        if ($this->_cron) {
            $this->_color = false;
            $this->_verbose = false;
        }
    }

    public function setRenderer(ARenderer $renderer) : static
    {
        $this->_renderer = $renderer;
        return $this;
    }

    public function getRenderer() : ARenderer
    {
        return $this->_renderer;
    }

    public function throwError(RuntimeException $e) : void
    {
        $this->logLn('ERROR: ' . $e->getMessage());
        if ($this->_cron) {
            fwrite(STDERR, $e->__toString());
        }
        $this->writeBoxMessage('<red>' . $e->__toString() . '</red>','red');
        exit(1);
    }

    public function setProperty(string $alias, ?string $regexp, mixed $default = null, bool $required = false, ?string $description = null) : static
    {
        $this->_parameters[$alias] = new Param($alias, $regexp, $default, $required, $description);
        return $this;
    }

    public function setRequiredProperty(string $alias, ?string $regexp = null, mixed $default = null, ?string $description = null) : static
    {
        $this->_parameters[$alias] = new Param($alias, $regexp, $default,true, $description);
        return $this;
    }

    public function setBoolProperty(string $alias, bool $default = false, bool $required = false, ?string $description = null) : static
    {
        $this->_parameters[$alias] = new Param($alias,null, $default, $required, $description);
        return $this;
    }

    public function parse(bool $require = true) : void
    {
        $clonedParams = $this->_params;
        while ($next = array_shift($clonedParams)) {
            $unknown = true;
            foreach ($this->_parameters as $param) {
                if ($param->setValue($next)) {
                    $unknown = false;
                    break;
                }
            }
            if ($unknown) $this->_unknownParams[] = $next;
        }
        $unsetRequired = array_filter($this->_parameters, [$this, 'checkRequiredParams']);
        if ($require && !empty($unsetRequired)) {
            $parameters = [];
            foreach ($unsetRequired as $required) {
                $parameters[] = implode('     ', $required->getParamConfig());
            }
            throw new RuntimeException('Please set required parameters: ' . PHP_EOL . implode(PHP_EOL, $parameters));
        }
    }

    public function __get(string $alias): mixed
    {
        return $this->_parameters[$alias]?->value;
    }

    public function __set(string $alias, mixed $value): void
    {
        if (isset($this->_parameters[$alias])) {
            $this->_parameters[$alias]->value = $value;
        }
    }

    public function __isset(string $alias) : bool
    {
        return isset($this->_parameters[$alias]);
    }

    public function writeHelp() : void
    {
        $verbose = $this->_verbose;
        $this->_verbose = true;
        $this->writeLn('Class: <green>' . static::class . '</green>');
        $this->writeLn('Command name: <green>' . static::getCallAs() . '</green>');
        $this->writeLn('Description: <green>' . static::getDescription() . '</green>');
        $this->getHelpUsage();
        $this->writeLn('Options: ');
        $this->_verbose = $verbose;
        $optionsResult = [];
        $optionValues = [];
        foreach ($this->_parameters as $param) {
            $optionsResult[] = $param->getParamConfig();
            $optionValues[] = $param->getParamConfig(true,true,true);
        }
        $this->_verbose = true;
        $this->writeTable($optionsResult);
        $this->writeLn('Changed option values:');
        $this->writeTable($optionValues,['cyan']);
    }

    public function getHelpUsage() : void
    {
        $this->writeLn('Usage: <green>' . static::getCallAs() . '</green>');
    }

    protected function checkRequiredParams(Param $param) : bool
    {
        return !$param->requireCheck();
    }
}