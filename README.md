### Instalation ###
Git clone
```
git clone git@bitbucket.org:Sezil/command.git Command
cd Command
cp command.example command
chmod +x command
```
Git submodule
```
git submodule add git@bitbucket.org:Sezil/command.git Modules/Command
cp Modules/Command/command.example command
chmod +x command
```

**After install**
 - run composer inside your project or in command project
 - check and update created php executable file 'command' for correct paths
 - create some project command directory (example in app/commands) then use method 'setCommandsDirectory' in file 'command' to add this custom commands folder

### Usage ###
```
./command <command> [options] #executes command
./command <namespace>.<command> [options] #executes command in namespace
./command <namespace>.<namespace>....<command> [options] #multiple namespaces separated by '.'(dot)
```

### Default commands ###
**commands**
```
./command commands #list available commands
./command commands <namespace> #list available commands for namespace
```
**help**
```
./command help <namespace> #list available commands for namespace
./command help <command> #shows help for command
./command help <namespace>.<command> #shows help for command in namespace
./command help <command> [options] #shows help for command in namespace with changed options
```
**bash-completion**
```
sudo ./command bash-completion #generates bash completion
```
**formats**
```
./command formats #show output format options
```

### Writing commands ###
in project custom commands directory create namespace directory with your custom name
(example `mkdir app/commands/scripts`). Create new php file in new directory (example `touch app/commands/scripts/MySuperScript.php`).
In that file create new PHP class with chosen namespace (example `Commands\Scripts`) that extends class **Command\Base\ABaseCommand**.
Fill required methods

* 'getCallAs' returns command name for usage
* 'getDescription' returns command description for help and output command headers
* 'execute' place for your special code that will be executed by running your command
* remove cache to load your new class, check if your command is available (listed) by running `./command commands` , update bash-completion

### Code-style ###
variables are prefixed with `_` to enable custom properties with the same name